<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-auteurs_syndic?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteurs_syndic_description' => 'Biedt de mogelijkheid om auteurs toe te voegen aan gesyndiceerde sites',
	'auteurs_syndic_nom' => 'Auteurs voor gesyndiceerde sites',
	'auteurs_syndic_slogan' => 'Biedt de mogelijkheid om auteurs toe te voegen aan gesyndiceerde sites'
);
