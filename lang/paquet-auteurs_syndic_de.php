<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-auteurs_syndic?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteurs_syndic_description' => 'Ermöglicht syndizierten Websites Autoren zuzuordnen',
	'auteurs_syndic_nom' => 'Autoren von syndizierten Websites',
	'auteurs_syndic_slogan' => 'Ermöglicht syndizierten Websites Autoren zuzuordnen'
);
